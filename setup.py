from setuptools import find_packages


try:
    from restricted_pkg import setup
except ImportError:
    from pip._internal import main
    main(['install', 'restricted_pkg'])
    from restricted_pkg import setup


setup(
    name='sebastian_bogy',
    version='0.1.0',
    description='A project for our Sebogy.',
    url='https://gitlab.com/Instaffo/DataScience/sebastian_bogy.git',
    author='Instaffo GmbH',
    author_email='info@instaffo.de',
    license='Proprietary',
    packages=find_packages(),
    package_data={},
    include_package_data=True,
    python_requires='>3.6, <3.7',
    private_repository='https://pypi.instaffo.tech',
    install_requires=[
        'restricted_pkg'
    ],
    zip_safe=False
)
